import { createServer } from 'http';

/**
 * Load App
 */
import app from './app';

const server = createServer(app);
const port: number = Number(3000);

(async () => {
  try {
    server.listen(port, () => {
      console.log(`Server is running on ${port}`);
    });
  } catch (e) {
    console.error(`Unable to connect to the server ${e}`);
    process.exit(1);
  }
})();
