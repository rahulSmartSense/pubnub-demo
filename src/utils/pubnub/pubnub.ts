import PubNub from 'pubnub';
import { v4 } from 'uuid';

console.log();
// Initializing pubnub instance and exporting
const pubnub = new PubNub({
  publishKey: 'pub-c-7df1dab5-8baa-40ca-a9b6-9e6a825c4590',
  subscribeKey: 'sub-c-ccb6f5fa-b15c-11ea-bf7c-ae6a3e72d19d',
  uuid: v4()
});

export { pubnub };
