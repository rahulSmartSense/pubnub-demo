import { CustomRequest, CustomResponse } from '../../environment';
import { pubnub } from '../../utils/pubnub';

class SubscriberController {
  public async subscribe(req: CustomRequest, res: CustomResponse) {
    try {
      console.log('Subscribing....');
      pubnub.subscribe({
        channels: ['channel-1']
      });
      return res.status(200).json({
        status: 200,
        data: ''
      });
    } catch (error) {
      console.error('Error :: ', error);
      return res.status(500).json({
        status: 500,
        error
      });
    }
  }
}

export default new SubscriberController();
