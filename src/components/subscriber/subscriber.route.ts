import { Request, Response, Router } from 'express';
import SubscriberController from './subscriber.controller';

const router = Router();

/**
 * @route POST /subscribe
 */
router.post('/subscribe', (req: Request, res: Response) => {
  SubscriberController.subscribe(req, res);
});

export default router;
