import { CustomRequest, CustomResponse } from '../../environment';
import { pubnub } from '../../utils/pubnub';

class PublisherController {
  public async publish(req: CustomRequest, res: CustomResponse) {
    try {
      const publishConfig = req.body;
      const data = await pubnub.publish(publishConfig);
      return res.status(200).json({
        status: 200,
        data
      });
    } catch (error) {
      console.error('Error :: ', error);
      return res.status(500).json({
        status: 500,
        error
      });
    }
  }
}

export default new PublisherController();
