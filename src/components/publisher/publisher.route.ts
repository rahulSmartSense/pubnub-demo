import { Request, Response, Router } from 'express';
import PublishController from './publisher.controller';

const router = Router();

/**
 * @route POST /publish
 */
router.post('/publish', (req: Request, res: Response) => {
  PublishController.publish(req, res);
});

export default router;
