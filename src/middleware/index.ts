import bodyParser from 'body-parser';
import { Application } from 'express';
import fileUpload from 'express-fileupload';

export default (app: Application) => {
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(fileUpload());
};
