import express, { Request, Response } from 'express';
import PublisherRoute from './components/publisher';
import SubscriberRoute from './components/subscriber';
import middleware from './middleware';

const app: express.Application = express();

middleware(app);

app.use('/api/publisher', PublisherRoute);
app.use('/api/subscriber', SubscriberRoute);

/**
 * Health Check
 * @route GET /health
 * @group Base - API of base routes
 * @returns {string} 200 - healthy
 */
app.get('/health', (req: Request, res: Response) => {
  return res.status(200).send('healthy');
});

// Invalid Route
app.all('/*', (req: Request, res: Response) => {
  return res.status(400).json({ status: 400, message: 'Bad Request' });
});

export default app;
