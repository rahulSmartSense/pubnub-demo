import { pubnub } from './utils/pubnub';

function subscriber() {
  pubnub.addListener({
    message: function(msg) {
      console.log(msg.message);
    }
  });
  console.log('Subscribing..');
  pubnub.subscribe({
    channels: ['channel-1']
  });
}

subscriber();
